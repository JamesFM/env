" begin vundle configuration
    set nocompatible
    " need to swtich to bash (or posix shell) if using fish
    set shell=bash
    filetype off
    set rtp+=~/.vim/bundle/vundle/
    call vundle#begin()

    " begin vundle bundles
        Plugin 'gmarik/vundle'

        " github bundles
        Plugin 'altercation/vim-colors-solarized'
        Plugin 'ctrlpvim/ctrlp.vim'
        Plugin 'scrooloose/nerdtree'
        Plugin 'pangloss/vim-javascript'
        Plugin 'kchmck/vim-coffee-script'
        Plugin 'rust-lang/rust.vim'
        " Plugin 'mutewinter/vim-indent-guides'
        Plugin 'leshill/vim-json'
        Plugin 'scrooloose/syntastic'
        Plugin 'scrooloose/nerdcommenter'
        Plugin 'tpope/vim-fugitive'
        Plugin 'tpope/vim-surround'
        Plugin 'ervandew/supertab'
        Plugin 'majutsushi/tagbar'
        Plugin 'Lokaltog/vim-powerline'
        Plugin 'terryma/vim-multiple-cursors'
        "Plugin 'Yggdroot/indentLine' "nice but SLOW
        " For sass
        Plugin 'tpope/vim-haml'
        Plugin 'vim-scripts/VimClojure'
        " ruby + rails (thanks James Daniels)
        Plugin 'vim-ruby/vim-ruby'
        Plugin 'tpope/vim-rails'
        Plugin 'fatih/vim-go' " go support
        Plugin 'chase/vim-ansible-yaml'
        Plugin 'sjbach/lusty'
        "Plugin 'Valloric/YouCompleteMe'

        " Haskell
        Plugin 'sdiehl/haskell-vim-proto'
        "Plugin 'eagletmt/ghcmod-vim'
        Plugin 'eagletmt/neco-ghc'
        Plugin 'alx741/vim-hindent'

        " Elm
        Plugin 'ElmCast/elm-vim'

        " Lisp
        Plugin 'bhurlow/vim-parinfer'

        " UltiSnips
            " Dependencies
            Plugin 'honza/vim-snippets'
            " the bundle itself
            Plugin 'SirVer/ultisnips'

        " non github bundles
        " Plugin 'git://git.wincent.com/command-t.git'

        " github/vim-scripts plugins
        Plugin 'matchit.zip'
        Plugin 'BufOnly.vim'
    " end vundle bundles
    call vundle#end()
    filetype plugin indent on
" end vundle configuration

" haskell
let g:haskellmode_completion_ghc = 1
autocmd FileType haskell setlocal omnifunc=necoghc#omnifunc

" elm
let g:elm_format_autosave = 1

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:elm_syntastic_show_warnings = 1

" additional configuration
set number
set colorcolumn=80
syntax enable
set smartindent
set tabstop=4
set shiftwidth=4
" tabs over spaces
set expandtab
set incsearch
set hlsearch
set mouse=a

" solarized theme settings
if !has('gui_running')
    " need to set this unless running the solarized colorscheme for the term
    " degrades solarized to 256 colors
    let g:solarized_termcolors=256
endif 
colorscheme solarized
set background=dark
if has('gui_running')
    set background=light
    set lines=48
    set columns=120
endif

" Higlight current line only in insert mode
autocmd InsertLeave * set nocursorline
autocmd InsertEnter * set cursorline
" Highlight cursor
if has('gui_running')
    highlight CursorLine ctermbg=8 cterm=NONE
else
    " color 8 is bad for my terminal
    highlight CursorLine ctermbg=16 cterm=NONE
endif

" omni completion (see http://amix.dk/blog/post/19021)
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType c set omnifunc=ccomplete#Complete

" Ruby auto completion options
autocmd Filetype ruby,eruby set omnifunc=rubycomplete#Complete
autocmd Filetype ruby,eruby let g:rubycomplete_buffer_loading = 1
autocmd Filetype ruby,eruby let g:rubycomplete_classes_in_global = 1
autocmd FileType ruby,eruby let g:rubycomplete_rails = 1
autocmd Filetype ruby,eruby setlocal ts=2 sts=2 sw=2

" coffeescript indentation
autocmd BufNewFile,BufReadPost *.coffee setl shiftwidth=2 expandtab

" wildmenu (command line completion) setup
set wildmenu
set wildmode=list:longest,full
set wildignore=*.o,*~,*.pyc,*.pyo,*.so,*.sw*,__pycache__

" nerdtree setup
" open nerdtree if no files are selected
autocmd vimenter * if !argc() | NERDTree | endif

" Syntastic
let g:syntastic_ruby_checkers = ['mri', 'rubocop']

" set hidden to prevent issues with lustyjuggler
:set hidden

" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"
